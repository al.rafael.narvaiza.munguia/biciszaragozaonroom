package com.iesportada.semipresencial.testretrofitroom.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;
import com.iesportada.semipresencial.testretrofitroom.repo.Repository;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {

    private Repository repo;
    private LiveData<List<Parking>> allParkings;
    public MainActivityViewModel(Application application){
        super(application);
        repo = new Repository(application);
        allParkings = repo.getParkings();
    }

    public void insert(Parking parking){
        repo.addParking(parking);
    }
    public void update(Parking parking){
        repo.updateParking(parking);
    }
    public void delete(Parking parking){
        repo.deleteParking(parking);
    }
    public LiveData<List<Parking>> getAllParkings(){
        return allParkings;
    }
}
