package com.iesportada.semipresencial.testretrofitroom.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Parking.class}, version = 1)
public abstract class ParkingDatabase extends RoomDatabase{
    private static ParkingDatabase INSTANCE;
    public abstract ParkingDao parkingDao();



    public static ParkingDatabase getInstance(Context context){
        if(INSTANCE== null) {
            synchronized (ParkingDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), ParkingDatabase.class, "estaciones")

                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);

            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
        private final ParkingDao parkingDao;
        PopulateDbAsync(ParkingDatabase db){
            parkingDao = db.parkingDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }
    }


}
