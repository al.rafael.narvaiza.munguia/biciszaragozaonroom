package com.iesportada.semipresencial.testretrofitroom.models.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("estado")
    @Expose
    private String estado;

    @SerializedName("bicisDisponibles")
    @Expose
    private String bicisDisponibles;

    @SerializedName("anclajesDisponibles")
    @Expose
    private String anclajesDisponibles;

    @SerializedName("geometry")
    @Expose
    private Geometry geometry;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getBicisDisponibles() {
        return bicisDisponibles;
    }

    public void setBicisDisponibles(String bicisDisponibles) {
        this.bicisDisponibles = bicisDisponibles;
    }

    public String getAnclajesDisponibles() {
        return anclajesDisponibles;
    }

    public void setAnclajesDisponibles(String anclajesDisponibles) {
        this.anclajesDisponibles = anclajesDisponibles;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

}
