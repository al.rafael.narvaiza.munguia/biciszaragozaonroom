package com.iesportada.semipresencial.testretrofitroom.models.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultList {

    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public List<Result> getResult() {
        return result;
    }
    public ResultList(){

    }

    public ResultList(List<Result> result){
        super();
        this.result=result;
    }

    public List<Result> getResults(){return result;}

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public ResultList withResult(List<Result> result){
        this.result=result;
        return this;
    }
}
