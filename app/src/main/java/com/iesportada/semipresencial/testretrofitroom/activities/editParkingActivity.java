package com.iesportada.semipresencial.testretrofitroom.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.iesportada.semipresencial.testretrofitroom.databinding.ActivityEditParkingBinding;

public class editParkingActivity extends AppCompatActivity {


    private ActivityEditParkingBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditParkingBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
    }
}