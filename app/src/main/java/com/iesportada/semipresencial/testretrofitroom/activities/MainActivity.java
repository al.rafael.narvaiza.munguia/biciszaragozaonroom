package com.iesportada.semipresencial.testretrofitroom.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.iesportada.semipresencial.testretrofitroom.R;
import com.iesportada.semipresencial.testretrofitroom.databinding.ActivityEditParkingBinding;
import com.iesportada.semipresencial.testretrofitroom.databinding.ActivityMainBinding;
import com.iesportada.semipresencial.testretrofitroom.models.retrofit.Geometry;
import com.iesportada.semipresencial.testretrofitroom.models.retrofit.Result;
import com.iesportada.semipresencial.testretrofitroom.models.retrofit.ResultList;
import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;
import com.iesportada.semipresencial.testretrofitroom.network.ApiAdapter;
import com.iesportada.semipresencial.testretrofitroom.recyclerview.ParkingAdapter;
import com.iesportada.semipresencial.testretrofitroom.recyclerview.RecyclerTouchListener;
import com.iesportada.semipresencial.testretrofitroom.viewmodel.MainActivityViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityMainBinding binding;
    ArrayList<Result> resultsList;
    List<Parking> parkingList;
    MainActivityViewModel viewModel;
    private ParkingAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        getParkings();
        initRecyclerView();
        initViewModel();

        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, binding.recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Parking parking = parkingList.get(position);
                Context context;
                AlertDialog ad = new Builder(MainActivity.this).setPositiveButton("Localizacion", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                        intent.putExtra("id", parking.getId());
                        intent.putExtra("nombre", parking.getNombre());
                        intent.putExtra("estado", parking.getEstado());
                        intent.putExtra("bicisDisponibles", parking.getBicisDisponibles());
                        intent.putExtra("anclajesDisponibles", parking.getAnclajesDisponibles());
                        intent.putExtra("coordenadas", parking.getCoordenadas());
                        startActivity(intent);
                    }
                }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setTitle("Mostrar localización").setMessage("Desea ir a la localización de la estación " + parking.getNombre() + " ?").create();
                ad.show();
            }

            //Delete from db
            @Override
            public void onLongClick(View view, int position) {
                Parking parking = parkingList.get(position);

                AlertDialog ad = new AlertDialog.Builder(MainActivity.this).setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewModel.delete(parking);
                        updateParkings();
                    }
                })
                        .setNegativeButton("cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setTitle("Eliminar parking")
                        .setMessage("Desea eliminar el parking seleccionado?")
                        .create();
                ad.show();
            }
        }));

        updateParkings();
        binding.fab.setOnClickListener(this);

    }

    private void initViewModel() {
        viewModel= new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.getAllParkings().observe(this, new Observer<List<Parking>>() {
            @Override
            public void onChanged(List<Parking> parkings) {
                adapter.setParkings(parkings);
                setParkingList(parkings);
            }
        });
    }

    private void initRecyclerView() {

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ParkingAdapter();
        binding.recyclerView.setAdapter(adapter);

    }

    public List<Parking> getParkingList(){
        return parkingList;
    }
    public void setParkingList(List<Parking> parkingList){
        this.parkingList=parkingList;
    }

    private void getParkings() {
        Call<ResultList> resultListCall = ApiAdapter.getInstance().getAllParkings();
        resultListCall.enqueue(new Callback<ResultList>() {
            @Override
            public void onResponse(Call<ResultList> call, Response<ResultList> response) {
                if (response.isSuccessful()){
                    List<ResultList> parkings = Collections.singletonList(response.body());
                    resultsList=new ArrayList<>();
                    for(ResultList rl : parkings){
                        resultsList= (ArrayList<Result>) rl.getResults();
                        for(Result r: resultsList){
                            Parking parking = new Parking();
                            parking.setId(Integer.valueOf(r.getId()));
                            parking.setNombre(r.getTitle());
                            parking.setBicisDisponibles(r.getBicisDisponibles());
                            parking.setAnclajesDisponibles(r.getAnclajesDisponibles());
                            parking.setCoordenadas(r.getGeometry().getCoordinates().get(1) + "," + r.getGeometry().getCoordinates().get(0));
                            Log.e("PArking", parking.getNombre()+parking.getCoordenadas());
                            List<Parking> savedParkings = getParkingList();
                            if (savedParkings.size()<=1){
                                viewModel.insert(parking);
                            }
                            else{
                                if(!isOnList(parking)){
                                    viewModel.insert(parking);
                                }

                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<ResultList> call, Throwable t) {
                Log.e("error", t.getLocalizedMessage());
                showMessage(t.getLocalizedMessage());
            }
        });
    }

    @Override
    protected void onPostResume(){
        super.onPostResume();
        viewModel.getAllParkings();
        updateParkings();
    }

    private void showMessage(String s){
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
    }
    private void updateParkings(){
        if(adapter==null) return;
        List<Parking> parkings = getParkingList();
        adapter.setParkings(parkings);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {
        if(v==binding.fab){
            Intent intent = new Intent(MainActivity.this, AddParkingActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_barra, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId()==R.id.itemRefresh){
            updateParkings();

        }
        return true;
    }

    public boolean isOnList(Parking parking){
        boolean exist= false;
        List<Parking> parkingList;
        parkingList=getParkingList();
        for(Parking p : parkingList){
            if(p.getNombre().equalsIgnoreCase(parking.getNombre())){
                exist = true;
            }
        }
        return exist;
    }
}