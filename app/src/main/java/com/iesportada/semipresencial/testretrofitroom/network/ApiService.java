package com.iesportada.semipresencial.testretrofitroom.network;

import com.iesportada.semipresencial.testretrofitroom.models.retrofit.ResultList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ApiService {
    @Headers({"Accept: application/json"})
    @GET("urbanismo-infraestructuras/estacion-bicicleta.json?rf=html&start=0&rows=130")
    Call<ResultList> getAllParkings();
}
