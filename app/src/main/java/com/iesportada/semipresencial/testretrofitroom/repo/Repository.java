package com.iesportada.semipresencial.testretrofitroom.repo;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.iesportada.semipresencial.testretrofitroom.db.ParkingDao;
import com.iesportada.semipresencial.testretrofitroom.db.ParkingDatabase;
import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;

import java.util.List;

public class Repository {
    @SuppressLint("StaticFieldLeak")
    public ParkingDao parkingDao;
    private LiveData<List<Parking>> parkingCollection;


    //TODO cambia esto a un Future como lo tienes en el test.
    public Repository(Application application){
        ParkingDatabase parkingDatabase = ParkingDatabase.getInstance(application);
        parkingDao=parkingDatabase.parkingDao();
        parkingCollection = parkingDao.getParkings();
    }

    public LiveData<List<Parking>> getParkings(){
        return parkingCollection;
    }

    public void addParking(Parking parking){
        new InsertAsync(parkingDao).execute(parking);
    }

    public void updateParking(Parking parking){
        new UpdateAsync(parkingDao).execute(parking);
    }

    public void deleteParking(Parking parking){
        new DeleteAsync(parkingDao).execute(parking);
    }

    //TODO cambialo a executeService
    //insert
    private static class InsertAsync extends AsyncTask<Parking, Void, Void>{
        ParkingDao parkingDao;
        public InsertAsync(ParkingDao parkingDao){
            this.parkingDao=parkingDao;
        }

        @Override
        protected Void doInBackground(Parking... park){
            parkingDao.addParking(park[0]);
            return null;
        }
    }

    //update
    private static class UpdateAsync extends AsyncTask<Parking, Void, Void>{
        ParkingDao parkingDao;
        public UpdateAsync(ParkingDao parkingDao){
            this.parkingDao=parkingDao;
        }

        @Override
        protected Void doInBackground(Parking... park){
            parkingDao.updateParking(park[0]);
            return null;
        }
    }

    //delete
    private static class DeleteAsync extends AsyncTask<Parking, Void, Void>{
        ParkingDao parkingDao;
        public DeleteAsync(ParkingDao parkingDao){
            this.parkingDao=parkingDao;
        }

        @Override
        protected Void doInBackground(Parking... park){
            parkingDao.deleteParking(park[0]);
            return null;
        }
    }
}
