package com.iesportada.semipresencial.testretrofitroom.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;
import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;
import java.util.List;

@Dao
public interface ParkingDao {
    @Transaction
    @Query(value = "SELECT * FROM parking ORDER BY id ")

    //TODO aquí debería de ir una lista normal, no un liveData,
    LiveData<List<Parking>> getParkings();
    @Transaction
    @Query("SELECT * FROM parking WHERE id LIKE :idParking")
    LiveData <Parking> getParking(Integer idParking);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addParking(Parking park);

    @Delete
    void deleteParking(Parking park);

    @Update
    void updateParking(Parking park);
}
