package com.iesportada.semipresencial.testretrofitroom.activities;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iesportada.semipresencial.testretrofitroom.R;
import com.iesportada.semipresencial.testretrofitroom.databinding.ActivityMapsBinding;

import org.jetbrains.annotations.NotNull;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;
    private LatLng parkingCoordinates;
    private String coordinates;
    private String parkingName;
    private String availableParkings;
    private String availabeBikes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        setParkingName(intent.getStringExtra("nombre"));
        setAvailabeBikes(intent.getStringExtra("bicisDisponibles"));
        setAvailableParkings(intent.getStringExtra("anclajesDisponibles"));
        setCoordinates(intent.getStringExtra("coordenadas"));
        coordinatesExchanger();

    }

    private void coordinatesExchanger() {
        LatLng latLng;
        String[] latLangCollection = getCoordinates().split(",");
        latLng = new LatLng(Double.valueOf(latLangCollection[0]), Double.valueOf(latLangCollection[1]));
        setParkingCoordinates(latLng);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.addMarker(new MarkerOptions().position(getParkingCoordinates()).title("Parking de :" + getParkingName()));
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(@NonNull @NotNull LatLng latLng) {
                showMessage(  "La estacion: " + getParkingName() + " tiene " + getAvailabeBikes() + " bicis disponibles y " +  getAvailableParkings() + " parkings disponibles.");

            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(getParkingCoordinates(), 15));
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
    }

    private void showMessage(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public LatLng getParkingCoordinates() {
        return parkingCoordinates;
    }

    public void setParkingCoordinates(LatLng parkingCoordinates) {
        this.parkingCoordinates = parkingCoordinates;
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName;
    }

    public String getAvailableParkings() {
        return availableParkings;
    }

    public void setAvailableParkings(String availableParkings) {
        this.availableParkings = availableParkings;
    }

    public String getAvailabeBikes() {
        return availabeBikes;
    }

    public void setAvailabeBikes(String availabeBikes) {
        this.availabeBikes = availabeBikes;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }
}