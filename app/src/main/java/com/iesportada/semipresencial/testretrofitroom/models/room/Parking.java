package com.iesportada.semipresencial.testretrofitroom.models.room;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.iesportada.semipresencial.testretrofitroom.models.retrofit.Geometry;

@Entity(tableName = "parking")
public class Parking {
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @ColumnInfo(name = "nombre")
    private String nombre;
    @ColumnInfo(name = "estado")
    private String estado;

    @ColumnInfo(name = "bicisDisponibles")
    private String bicisDisponibles;

    @ColumnInfo(name = "anclajesDisponibles")
    private String anclajesDisponibles;



    @ColumnInfo(name = "coordenadas")
    private String coordenadas;




    public Parking(Integer id) {
        this.id = id;
    }
    public Parking(){

    }

    public Parking(String nombre, String estado, String bicisDisponibles, String anclajesDisponibles, String coordenadas) {
        this.nombre = nombre;
        this.estado = estado;
        this.bicisDisponibles = bicisDisponibles;
        this.anclajesDisponibles=anclajesDisponibles;
        this.setCoordenadas(coordenadas);
    }

    @NonNull
    public Integer  getId() {
        return id;
    }

    public void setId(@NonNull Integer  id) {
        this.id = id;
    }

    @NonNull

    public String getNombre() {
        return nombre;
    }

    public void setNombre( String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public String getBicisDisponibles() {
        return bicisDisponibles;
    }

    public void setBicisDisponibles(String bicisDisponibles) {
        this.bicisDisponibles = bicisDisponibles;
    }

    public String getAnclajesDisponibles() {
        return anclajesDisponibles;
    }

    public void setAnclajesDisponibles(String anclajesDisponibles) {
        this.anclajesDisponibles = anclajesDisponibles;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }
}
