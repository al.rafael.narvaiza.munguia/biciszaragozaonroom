package com.iesportada.semipresencial.testretrofitroom.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iesportada.semipresencial.testretrofitroom.databinding.ItemViewBinding;
import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ParkingAdapter extends RecyclerView.Adapter<ParkingAdapter.MyViewHolder> {

    private Context context;
    public List<Parking> parkingList;

    public ParkingAdapter(){
        this.parkingList = new ArrayList<>();
    }
    public ParkingAdapter(Context context, List<Parking> listOfParkings){
        this.context=context;
        this.parkingList=listOfParkings;
    }
    public void setParkings(List<Parking> parkings){
        parkingList=parkings;
        notifyDataSetChanged();
    }
    @NonNull
    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {

        return new MyViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.getContext()),parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder holder, int position) {
        if(parkingList != null){
            Parking parking = parkingList.get(position);
            holder.binding.textEditId.setText(parking.getId().toString());
            holder.binding.textEditNombre.setText(parking.getNombre());
            holder.binding.textEditAnclajes.setText(parking.getAnclajesDisponibles().toString());
            holder.binding.textEditBicisDisponibles.setText(parking.getBicisDisponibles().toString());
        }
        else{
            holder.binding.textEditNombre.setText("Parking no disponible.");
        }

    }


    @Override
    public int getItemCount() {
        if(parkingList!=null){
            return parkingList.size();
        }
        else
            return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ItemViewBinding binding;

        public MyViewHolder(ItemViewBinding b) {
            super(b.getRoot());
            binding=b;
        }
    }
}
