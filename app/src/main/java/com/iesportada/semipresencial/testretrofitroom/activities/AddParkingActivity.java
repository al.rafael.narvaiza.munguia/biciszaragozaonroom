package com.iesportada.semipresencial.testretrofitroom.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.iesportada.semipresencial.testretrofitroom.databinding.ActivityAddParkingBinding;
import com.iesportada.semipresencial.testretrofitroom.models.room.Parking;
import com.iesportada.semipresencial.testretrofitroom.sandbox.Utils;
import com.iesportada.semipresencial.testretrofitroom.viewmodel.MainActivityViewModel;

import okhttp3.internal.Util;

public class AddParkingActivity extends AppCompatActivity {

    private ActivityAddParkingBinding binding;
    private MainActivityViewModel viewModel;
    Utils utils = new Utils();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityAddParkingBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        super.onCreate(savedInstanceState);
        setContentView(view);
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        binding.buttonDiscard.setOnClickListener(v->finish());
        binding.buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Parking parking = new Parking();
                String nombre, bicis, huecos, estado, coordenadas;
                nombre = binding.editTextParkingName.getText().toString();
                parking.setNombre(nombre);
                bicis = binding.editTextBicisDisponibles.getText().toString();
                parking.setBicisDisponibles(bicis);
                estado = binding.editTextTextParkingEstado.getText().toString();
                parking.setEstado(estado);
                huecos = binding.editTextTextAnclajesDisponibles.getText().toString();
                parking.setAnclajesDisponibles(huecos);
                coordenadas = "0.8979132352053497," + "41.64401875747108";
                parking.setCoordenadas(coordenadas);
                if(!Utils.inputFieldsAreChecked(nombre, bicis, estado, huecos,coordenadas).isEmpty()){
                    showMessage(Utils.inputFieldsAreChecked(nombre, bicis, estado, huecos, coordenadas));
                }
                else{
                    viewModel.insert(parking);
                    finish();
                }

            }
        });
        viewModel.getAllParkings();
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}